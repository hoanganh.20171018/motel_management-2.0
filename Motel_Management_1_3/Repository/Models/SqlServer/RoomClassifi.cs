﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Motel_Management_1_3.Repository.Models.SqlServer
{
    [Table("Room_Classifi")]
    public partial class RoomClassifi
    {
        public RoomClassifi()
        {
            Rooms = new HashSet<Room>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("_name")]
        [StringLength(32)]
        public string Name { get; set; }
        [Column("price")]
        public int Price { get; set; }

        [InverseProperty(nameof(Room.Classifi))]
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
