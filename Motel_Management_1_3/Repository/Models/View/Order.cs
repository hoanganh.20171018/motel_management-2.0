﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Repository.Models.View
{
    public class Order : SqlServer.Order
    {
        public new string ValidFrom { get; set; }
        public new string ExpiresEnd { get; set; }
    }
}
