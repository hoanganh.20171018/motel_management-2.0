﻿using System;
using System.Collections.Generic;
using Motel_Management_1_3.Repository.Models.Input;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Services.BLL
{
    interface Prototype<Tin, Tout>
    {
        bool isValid(int input);
        List<Tout> AllRecords();
        List<Tout> SomeRecords(List<Filter> filters);
        Tout Insert(Tin input);
        Tout Update(Tin input);
        Tout Delete(Tin input);
    }
}
