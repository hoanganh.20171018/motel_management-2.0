﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Repository.Models.Input
{
    public class Filter
    {
        public string Field { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public object ToNative()
        {
            switch ($"{Type}".ToLower())
            {
                case "int": return int.TryParse(Value, out int intValue) ? intValue : -1;
                case "datetime": return DateTime.TryParse(Value, out DateTime dateTimeValues) ? dateTimeValues : new DateTime();
                case "string": return $"{Value}";
                default: return null;
            }
        }
    }
}
