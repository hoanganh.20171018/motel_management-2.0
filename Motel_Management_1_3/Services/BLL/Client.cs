﻿using Motel_Management_1_3.Repository;
using Motel_Management_1_3.Repository.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Services.BLL
{
    public class Client : Prototype<Repository.Models.SqlServer.Client, Repository.Models.SqlServer.Client>
    {
        public bool isValid(int input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    return help.Clients.Where(client => client.Id == input).Count() == 1;
                }
                catch
                {
                    throw;
                }
        }

        public List<Repository.Models.SqlServer.Client> AllRecords()
        {
            using (var help = new MotelDbContext())
                return help.Clients.ToList();
        }

        public List<Repository.Models.SqlServer.Client> SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.Client> result = new List<Repository.Models.SqlServer.Client>();

            if (filters is null)
                return result;

            Dictionary<string, object> extras =
            filters.ToDictionary(
                    x => x.Field,
                    x => x.ToNative()
                );

            using (var help = new MotelDbContext())
                foreach (var extra in extras)
                {
                    if (extra.Key == "id")
                        result.AddRange(help.Clients.Where(x => x.Id == (int)extra.Value));
                    else if (extra.Key == "name")
                        result.AddRange(help.Clients.Where(x => x.Name == (string)extra.Value));
                    else if (extra.Key == "passport")
                        result.AddRange(help.Clients.Where(x => x.PassPort == (string)extra.Value));
                    else if (extra.Key == "phonenumber")
                        result.AddRange(help.Clients.Where(x => x.PhoneNumber == (string)extra.Value));
                    else
                        continue;
                }

            return result.GroupBy(x => x.Id).Select(fs => fs.First()).ToList();
        }

        public Repository.Models.SqlServer.Client Insert(Repository.Models.SqlServer.Client input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Clients.Add(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return input;
        }

        public Repository.Models.SqlServer.Client Update(Repository.Models.SqlServer.Client input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Clients.Update(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return input;
        }

        public Repository.Models.SqlServer.Client Delete(Repository.Models.SqlServer.Client input)
        {
            using (var help = new MotelDbContext())
            {
                using (Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = help.Database.BeginTransaction())
                {
                    try
                    {
                        help.Orders.RemoveRange(help.Orders.Where(x => x.ClientId == input.Id));
                        help.SaveChanges();

                        help.Clients.Remove(input);
                        help.SaveChanges();

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            }

            return input;
        }
    }
}
