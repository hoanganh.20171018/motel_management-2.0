﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Motel_Management_1_3.Repository.Models.SqlServer
{
    [Table("Room")]
    public partial class Room
    {
        public Room()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("_name")]
        [StringLength(32)]
        public string Name { get; set; }
        [Column("classifiID")]
        public int ClassifiId { get; set; }

        [ForeignKey(nameof(ClassifiId))]
        [InverseProperty(nameof(RoomClassifi.Rooms))]
        public virtual RoomClassifi Classifi { get; set; }
        [InverseProperty(nameof(Order.Room))]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
