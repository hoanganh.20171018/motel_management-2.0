﻿using Microsoft.AspNetCore.Mvc;
using Motel_Management_1_3.Repository.Models.Input;
using Motel_Management_1_3.Repository.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : Controller, Prototype<Repository.Models.SqlServer.Order>
    {
        [HttpGet("AllRecords")]
        public IActionResult AllRecords()
        {
            List<Order> data = null;

            try
            {
                data = new Services.BLL.Order().AllRecords();

                if (data.Count == 0)
                    return Ok(View<List<Order>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Order>>
                    .False(data, "Lỗi", exception));
            }
            
            return Ok(View<List<Order>>
                .True(data));
        }

        [HttpPost("SomeRecords/{List<Filter>}")]
        public IActionResult SomeRecords(List<Filter> filters)
        {
            List<Order> data = null;

            try
            {
                data = new Services.BLL.Order().SomeRecords(filters);

                if (data.Count == 0)
                    return Ok(View<List<Order>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Order>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Order>>
                .True(data));
        }

        [HttpPost("Insert/{Repository.Models.SqlServer.Order}")]
        public IActionResult Insert(Repository.Models.SqlServer.Order input)
        {
            Order data = null;

            try
            {
                if (!new Services.BLL.Client().isValid(input.ClientId))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại Khách Hàng!", null));
                if (!new Services.BLL.Room().isValid(input.RoomId))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại Phòng!", null));

                if (!Services.BLL.Order.Allow(input.RoomId, input.ValidFrom))
                    return Ok(View<Order>
                        .False(data, "Phòng Đã Có Khách!", null));

                data = new Services.BLL.Order().Insert(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Order>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Order>
                .True(data));
        }

        [HttpPut("Update/{Repository.Models.SqlServer.Order}")]
        public IActionResult Update(Repository.Models.SqlServer.Order input)
        {
            Order data = null;

            try
            {
                if (!new Services.BLL.Order().isValid(input.Id))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại!", null));
                if (!new Services.BLL.Client().isValid(input.ClientId))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại Khách Hàng!", null));
                if (!new Services.BLL.Room().isValid(input.RoomId))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại Phòng!", null));
                if (!Services.BLL.Order.Allow(input.RoomId, input.ValidFrom))
                    return Ok(View<Order>
                        .False(data, "Phòng Đã Có Khách!", null));

                data = new Services.BLL.Order().Update(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Order>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Order>
                .True(data));
        }

        [HttpDelete("Delete/{Repository.Models.SqlServer.Order}")]
        public IActionResult Delete(Repository.Models.SqlServer.Order input)
        {
            Order data = null;

            try
            {
                if (!new Services.BLL.Order().isValid(input.Id))
                    return Ok(View<Order>
                        .False(data, "Không Tồn Tại!", null));

                data = new Services.BLL.Order().Delete(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Order>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Order>
                .True(data));
        }
    }
}
