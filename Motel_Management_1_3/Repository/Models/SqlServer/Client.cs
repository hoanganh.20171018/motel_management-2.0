﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Motel_Management_1_3.Repository.Models.SqlServer
{
    [Table("Client")]
    public partial class Client
    {
        public Client()
        {
            Orders = new HashSet<Order>();
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Required]
        [Column("_name")]
        [StringLength(128)]
        public string Name { get; set; }
        [Required]
        [Column("passPort")]
        [StringLength(16)]
        public string PassPort { get; set; }
        [Required]
        [Column("phoneNumber")]
        [StringLength(16)]
        public string PhoneNumber { get; set; }

        [InverseProperty(nameof(Order.Client))]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
