﻿using Motel_Management_1_3.Repository;
using Motel_Management_1_3.Repository.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Services.BLL
{
    public class Room : Prototype<Repository.Models.SqlServer.Room, Repository.Models.SqlServer.Room>
    {
        public bool isValid(int input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    return help.Rooms.Where(room => room.Id == input).Count() == 1;
                }
                catch
                {
                    throw;
                }
        }

        public List<Repository.Models.SqlServer.Room> AllRecords()
        {
            using (var help = new MotelDbContext())
                return help.Rooms.ToList();
        }

        public List<Repository.Models.SqlServer.Room> SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.Room> result = new List<Repository.Models.SqlServer.Room>();

            if (filters is null)
                return result;

            Dictionary<string, object> extras =
            filters.ToDictionary(
                    x => x.Field,
                    x => x.ToNative()
                );

            using (var help = new MotelDbContext())
                foreach (var extra in extras)
                {
                    if (extra.Key == "id")
                        result.AddRange(help.Rooms.Where(x => x.Id == (int)extra.Value));
                    else if (extra.Key == "name")
                        result.AddRange(help.Rooms.Where(x => x.Name == (string)extra.Value));
                    else if (extra.Key == "classifiid")
                        result.AddRange(help.Rooms.Where(x => x.ClassifiId == (int)extra.Value));
                    else
                        continue;
                }

            return result.GroupBy(x => x.Id).Select(fs => fs.First()).ToList();
        }

        public Repository.Models.SqlServer.Room Insert(Repository.Models.SqlServer.Room input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Rooms.Add(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return input;
        }

        public Repository.Models.SqlServer.Room Update(Repository.Models.SqlServer.Room input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Rooms.Update(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return input;
        }

        public Repository.Models.SqlServer.Room Delete(Repository.Models.SqlServer.Room input)
        {
            using (var help = new MotelDbContext())
            {
                using (Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = help.Database.BeginTransaction())
                {
                    try
                    {
                        help.Orders.RemoveRange(help.Orders.Where(x => x.RoomId == input.Id));
                        help.SaveChanges();

                        help.Rooms.Remove(input);
                        help.SaveChanges();

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            }

            return input;
        }
    }
}
