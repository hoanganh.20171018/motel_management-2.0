﻿using System;
using System.Collections.Generic;
using Motel_Management_1_3.Repository.Models.Input;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Controllers
{
    interface Prototype<T>
    {
        IActionResult AllRecords();
        IActionResult SomeRecords(List<Filter> filters);
        IActionResult Insert(T input);
        IActionResult Update(T input);
        IActionResult Delete(T input);
    }
}
