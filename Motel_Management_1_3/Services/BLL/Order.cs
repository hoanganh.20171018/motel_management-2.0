﻿using Motel_Management_1_3.Repository;
using Motel_Management_1_3.Repository.Models.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Services.BLL
{
    public class Order : Prototype<Repository.Models.SqlServer.Order, Repository.Models.View.Order>
    {
        private List<Repository.Models.View.Order> ToView(List<Repository.Models.SqlServer.Order> converter)
        {
            List<Repository.Models.View.Order> result = new List<Repository.Models.View.Order>();

            foreach (Repository.Models.SqlServer.Order input in converter)
                result.Add(new Repository.Models.View.Order()
                {
                    Id = input.Id,
                    ClientId = input.ClientId,
                    RoomId = input.RoomId,
                    ValidFrom = input.ValidFrom.ToShortDateString(),
                    ExpiresEnd = $"{input.ExpiresEnd?.ToShortDateString()}"
                });

            return result;
        }
        private Repository.Models.View.Order ToView(Repository.Models.SqlServer.Order converter)
        {
            return new Repository.Models.View.Order()
            {
                Id = converter.Id,
                ClientId = converter.ClientId,
                RoomId = converter.RoomId,
                ValidFrom = converter.ValidFrom.ToShortDateString(),
                ExpiresEnd = $"{converter.ExpiresEnd?.ToShortDateString()}"
            };
        }
        
        public bool isValid(int input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    return help.Orders.Where(order => order.Id == input).Count() == 1;
                }
                catch
                {
                    throw;
                }
        }

        public static bool Allow(int roomId, DateTime validFrom)
        {
            using (var help = new MotelDbContext())
                return help.Orders.Where(x => x.RoomId == roomId &&
                    (x.ExpiresEnd >= validFrom || !x.ExpiresEnd.HasValue)).ToList().Count() == 0;
        }

        public List<Repository.Models.View.Order> AllRecords()
        {
            using (var help = new MotelDbContext())
                return ToView(help.Orders.ToList());
        }

        public List<Repository.Models.View.Order> SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.Order> assistances = new List<Repository.Models.SqlServer.Order>();

            if (filters is null)
                return new List<Repository.Models.View.Order>();

            Dictionary<string, object> extras =
            filters.ToDictionary(
                    x => x.Field,
                    x => x.ToNative()
                );

            using (var help = new MotelDbContext())
                foreach (var extra in extras)
                {
                    if (extra.Key == "id")
                        assistances.AddRange(help.Orders.Where(x => x.Id == (int)extra.Value).ToList());
                    else if (extra.Key == "clientid")
                        assistances.AddRange(help.Orders.Where(x => x.ClientId == (int)extra.Value));
                    else if (extra.Key == "roomid")
                        assistances.AddRange(help.Orders.Where(x => x.RoomId == (int)extra.Value));
                    else if (extra.Key == "validfrom")
                        assistances.AddRange(help.Orders.Where(x => x.ValidFrom == (DateTime)extra.Value));
                    else if (extra.Key == "expiresend")
                        assistances.AddRange(help.Orders.Where(x => x.ExpiresEnd == (DateTime)extra.Value));
                    else
                        continue;
                }

            return ToView(assistances).GroupBy(x => x.Id).Select(fs => fs.First()).ToList();
        }

        public Repository.Models.View.Order Insert(Repository.Models.SqlServer.Order input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Orders.Add(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return ToView(input);
        }

        public Repository.Models.View.Order Update(Repository.Models.SqlServer.Order input)
        {
            using (var help = new MotelDbContext())
                try
                {
                    help.Orders.Update(input);
                    help.SaveChanges();
                }
                catch
                {
                    throw;
                }

            return ToView(input);
        }

        public Repository.Models.View.Order Delete(Repository.Models.SqlServer.Order input)
        {
            using (var help = new MotelDbContext())
            {
                using (Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction transaction = help.Database.BeginTransaction())
                {
                    try
                    {
                        help.Orders.Remove(input);
                        help.SaveChanges();

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            }

            return ToView(input);
        }
    }
}
