﻿using Microsoft.AspNetCore.Mvc;
using Motel_Management_1_3.Repository.Models.Input;
using Motel_Management_1_3.Repository.Models.View;
using Motel_Management_1_3.Services.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientController : Controller, Prototype<Repository.Models.SqlServer.Client>
    {
        [HttpGet("AllRecords")]
        public IActionResult AllRecords()
        {
            List<Repository.Models.SqlServer.Client> data = null;

            try
            {
                data = new Client().AllRecords();

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.Client>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.Client>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.Client>>
                .True(data));
        }

        [HttpPost("SomeRecords/{List<Filter>}")]
        public IActionResult SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.Client> data = null;

            try
            {
                data = new Client().SomeRecords(filters);

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.Client>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.Client>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.Client>>
                .True(data));
        }

        [HttpPost("Insert/{Repository.Models.SqlServer.Client}")]
        public IActionResult Insert(Repository.Models.SqlServer.Client input)
        {
            Repository.Models.SqlServer.Client data = null;

            try
            {
                data = new Client().Insert(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Client>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Client>
                .True(data));
        }

        [HttpPut("Update/{Repository.Models.SqlServer.Client}")]
        public IActionResult Update(Repository.Models.SqlServer.Client input)
        {
            Repository.Models.SqlServer.Client data = null;

            try
            {
                if (!new Client().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.Client>
                        .False(data, "Không Tồn Tại!", null));

                data = new Client().Update(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Client>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Client>
                .True(data));
        }

        [HttpDelete("Delete/{Repository.Models.SqlServer.Client}")]
        public IActionResult Delete(Repository.Models.SqlServer.Client input)
        {
            Repository.Models.SqlServer.Client data = null;

            try
            {
                if (!new Client().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.Client>
                        .False(data, "Không Tồn Tại!", null));

                data = new Client().Delete(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Client>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Client>
                .True(data));
        }
    }
}
