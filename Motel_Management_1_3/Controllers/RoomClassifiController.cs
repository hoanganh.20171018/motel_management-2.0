﻿using Microsoft.AspNetCore.Mvc;
using Motel_Management_1_3.Repository.Models.Input;
using Motel_Management_1_3.Repository.Models.View;
using Motel_Management_1_3.Services.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomClassifiController : Controller, Prototype<Repository.Models.SqlServer.RoomClassifi>
    {
        [HttpGet("AllRecords")]
        public IActionResult AllRecords()
        {
            List<Repository.Models.SqlServer.RoomClassifi> data = null;

            try
            {
                data = new RoomClassifi().AllRecords();

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                .True(data));
        }

        [HttpPost("SomeRecords/{List<Filter>}")]
        public IActionResult SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.RoomClassifi> data = null;

            try
            {
                data = new RoomClassifi().SomeRecords(filters);

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.RoomClassifi>>
                .True(data));
        }

        [HttpPost("Insert/{Repository.Models.SqlServer.RoomClassifi}")]
        public IActionResult Insert(Repository.Models.SqlServer.RoomClassifi input)
        {
            Repository.Models.SqlServer.RoomClassifi data = null;

            try
            {
                data = new RoomClassifi().Insert(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                .True(data));
        }

        [HttpPut("Update/{Repository.Models.SqlServer.RoomClassifi}")]
        public IActionResult Update(Repository.Models.SqlServer.RoomClassifi input)
        {
            Repository.Models.SqlServer.RoomClassifi data = null;

            try
            {
                if (!new RoomClassifi().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                        .False(data, "Không Tồn Tại!", null));

                data = new RoomClassifi().Update(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                .True(data));
        }

        [HttpDelete("Delete/{Repository.Models.SqlServer.RoomClassifi}")]
        public IActionResult Delete(Repository.Models.SqlServer.RoomClassifi input)
        {
            Repository.Models.SqlServer.RoomClassifi data = null;

            try
            {
                if (!new RoomClassifi().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                        .False(data, "Không Tồn Tại!", null));

                data = new RoomClassifi().Delete(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.RoomClassifi>
                .True(data));
        }
    }
}
