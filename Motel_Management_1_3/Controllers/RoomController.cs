﻿using Microsoft.AspNetCore.Mvc;
using Motel_Management_1_3.Repository.Models.Input;
using Motel_Management_1_3.Repository.Models.View;
using Motel_Management_1_3.Services.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Motel_Management_1_3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomController : Controller, Prototype<Repository.Models.SqlServer.Room>
    {
        [HttpGet("AllRecords")]
        public IActionResult AllRecords()
        {
            List<Repository.Models.SqlServer.Room> data = null;

            try
            {
                data = new Room().AllRecords();

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.Room>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.Room>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.Room>>
                .True(data));
        }

        [HttpPost("SomeRecords/{List<Filter>}")]
        public IActionResult SomeRecords(List<Filter> filters)
        {
            List<Repository.Models.SqlServer.Room> data = null;

            try
            {
                data = new Room().SomeRecords(filters);

                if (data.Count == 0)
                    return Ok(View<List<Repository.Models.SqlServer.Room>>
                        .False(data, "Không Tồn Tại Danh Sách", null));
            }
            catch (Exception exception)
            {
                return Ok(View<List<Repository.Models.SqlServer.Room>>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<List<Repository.Models.SqlServer.Room>>
                .True(data));
        }

        [HttpPost("Insert/{Repository.Models.SqlServer.Room}")]
        public IActionResult Insert(Repository.Models.SqlServer.Room input)
        {
            Repository.Models.SqlServer.Room data = null;

            try
            {
                if (!new RoomClassifi().isValid(input.ClassifiId))
                    return Ok(View<Repository.Models.SqlServer.Room>
                        .False(data, "Không Tồn Tại Loại Phòng!", null));

                data = new Room().Insert(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Room>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Room>
                .True(data));
        }

        [HttpPut("Update/{Repository.Models.SqlServer.Room}")]
        public IActionResult Update(Repository.Models.SqlServer.Room input)
        {
            Repository.Models.SqlServer.Room data = null;

            try
            {
                if (!new Room().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.Room>
                        .False(data, "Không Tồn Tại!", null));
                if (!new RoomClassifi().isValid(input.ClassifiId))
                    return Ok(View<Repository.Models.SqlServer.Room>
                        .False(data, "Không Tồn Tại Loại Phòng!", null));

                data = new Room().Update(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Room>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Room>
                .True(data));
        }

        [HttpDelete("Delete/{Repository.Models.SqlServer.Room}")]
        public IActionResult Delete(Repository.Models.SqlServer.Room input)
        {
            Repository.Models.SqlServer.Room data = null;

            try
            {
                if (!new Room().isValid(input.Id))
                    return Ok(View<Repository.Models.SqlServer.Room>
                        .False(data, "Không Tồn Tại!", null));

                data = new Room().Delete(input);
            }
            catch (Exception exception)
            {
                return Ok(View<Repository.Models.SqlServer.Room>
                    .False(data, "Lỗi", exception));
            }

            return Ok(View<Repository.Models.SqlServer.Room>
                .True(data));
        }
    }
}
