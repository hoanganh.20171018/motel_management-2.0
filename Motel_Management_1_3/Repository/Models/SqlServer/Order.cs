﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Motel_Management_1_3.Repository.Models.SqlServer
{
    [Table("_Order")]
    public partial class Order
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("clientID")]
        public int ClientId { get; set; }
        [Column("roomID")]
        public int RoomId { get; set; }
        [Column("validFrom", TypeName = "date")]
        public DateTime ValidFrom { get; set; }
        [Column("expiresEnd", TypeName = "date")]
        public DateTime? ExpiresEnd { get; set; }

        [ForeignKey(nameof(ClientId))]
        [InverseProperty("Orders")]
        public virtual Client Client { get; set; }
        [ForeignKey(nameof(RoomId))]
        [InverseProperty("Orders")]
        public virtual Room Room { get; set; }
    }
}
